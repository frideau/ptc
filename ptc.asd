;;; -*- mode: lisp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                  ;;;
;;; Free Software available under an MIT-style license.              ;;;
;;;                                                                  ;;;
;;; Copyright (c) 2010-2023 Francois-Rene Rideau                     ;;;
;;;                                                                  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defsystem "ptc"
  :version "1.0.2"
  :author ("Francois-Rene Rideau")
  :licence "MIT"
  :description "Proper Tail Calls for CL"
  :long-description "Portably enable Proper Tail Calls in Common Lisp."
  :depends-on ()
  :serial t
  :components
  ((:file "ptc")))
