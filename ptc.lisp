(cl:defpackage :proper-tail-calls
  (:nicknames :ptc)
  (:use :cl)
  (:export
   #:=declare-proper-tail-calls=
   #:declaim-proper-tail-calls
   #:proclaim-proper-tail-calls
   #:with-proper-tail-calls))

(in-package :ptc)

(eval-when (:compile-toplevel :load-toplevel :execute)

(defvar =enable-proper-tail-calls=
  #+allegro
  '((speed 3))
  ;; I've never really tried with Allegro, but this recipe was suggested by:
  ;; https://github.com/rmoritz/trivial-tco/blob/master/src/trivial-tco.lisp

  #+clozure
  '((debug 2))
  ;; Any debug not 3 will usually do -- thanks to rme & gbyers
  ;; http://trac.clozure.com/ccl/wiki/DeclareOptimize
  ;; http://paste.lisp.org/display/94457

  #+lispworks
  '((debug 0))
  ;; I've never really tried with LispWorks, but this recipe was suggested by:
  ;; https://github.com/rmoritz/trivial-tco/blob/master/src/trivial-tco.lisp

  #+sbcl
  '((speed 3) (sb-c::insert-debug-catch 0))
  ;; Unless we have speed 3, the other may not apply
  ;; (sb-c::merge-tail-calls 3) is deprecated as of 1.0.53.74
  ;; it was never useful, for SBCL always merge tail calls when it can.

  #-(or allegro clozure lispworks sbcl)
  '((speed 3) (debug 1)) ;; best effort attempt

  "Not recommended for direct use")

(defvar =optimize-proper-tail-calls= `(optimize ,@=enable-proper-tail-calls=)
  "Not recommended for direct use")

(defvar =declare-proper-tail-calls= `(declare ,=optimize-proper-tail-calls=))

(defmacro declaim-proper-tail-calls ()
  #+clozure
  '(eval-when (:compile-toplevel :execute)
    (proclaim-proper-tail-calls))
  #-clozure
  `(declaim ,=optimize-proper-tail-calls=))

(defun proclaim-proper-tail-calls ()
  #+clozure
  (setf ccl::*compile-time-evaluation-policy*
        (ccl::new-compiler-policy :allow-tail-recursion-elimination (constantly t)))
  ;; see also ccl::*default-compiler-policy* ccl::*default-file-compilation-policy*

  #-clozure
  (proclaim =optimize-proper-tail-calls=)))

(defmacro with-proper-tail-calls (() &body body)
  `(locally ,=declare-proper-tail-calls=
     ,@body))

#-(or allegro clozure lispworks sbcl)
(format t "~&Your implementation is not supported by the CL PTC library.
We're providing a best effort with (speed 3) (debug 1).
If you know how to do things properly with your Lisp implementation,
please send mail to the author <fahree at gmail>.~%")
